'use strict'
const express = require('express');
const bodyParser = require('body-parser');


// Express application
const app = express();

// Flat Data (FakeDataBase)
const {data} = require('./data/csaData.json');
const {profiles} = data;
const profilesLength = profiles.length;

// console.log(data.profiles[0].ID)
// console.log(data.profiles[0].name)
// console.log(profilesLength);

// MiddleWare
app.use(bodyParser.urlencoded({ extended:true }));

// Set the app to use pug for templating
app.set('view engine', 'pug');

// Serving the home.pug template to the root route

app.get('/', (req, res) => {

    res.render('home');
})

// Serving the search.pug template to the /search route
app.get('/search', (req, res) => {

    res.render('search');
})

app.post('/search', (req, res) => {

    const userInput = req.body.username.toLowerCase(); // username ofr the name='username' set in the form

    // Validation Logics
    
    // ID and Username validation
    if (/^[0-9]{9}$/.test(userInput) || /^[A-Z]?[a-z]+$/.test(userInput)) {

        console.log('Valid ID or Username');

        for ( let profileIndex = 0; profileIndex < profilesLength; profileIndex++ ){

            if( profiles[profileIndex].ID === userInput ){

                return res.render('search', { name: profiles[profileIndex].name});
            
            } else if ( profiles[profileIndex].name.toLowerCase().includes(userInput)) {

                return res.render('search', {name: profiles[profileIndex].name});
            } 

        } 

        // Serving the noresult.pug template
        return res.render('noresult', { name: userInput });
       

   
    // Wrong input validation
    } else {

        console.log('Invalid ID or Username');
        return res.render('invalid');
    }

})

// Development server on port 3000
app.listen(3000, () => {

    console.log("The application is running on port: 3000!")
});